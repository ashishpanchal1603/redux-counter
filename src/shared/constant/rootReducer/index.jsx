import { combineReducers } from 'redux'
import changeValue from '../reducer/index'
const rootReducer = combineReducers({
  changeValue
})

export default rootReducer
