import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { incNumber, decNumber } from '../action'
function CounterFunction () {
  const myState = useSelector((state) => state.changeValue)
  const dispatch = useDispatch()
  return (
    <>
     <button onClick={() => dispatch(incNumber())}>increment</button>
    <h1>{myState}</h1>
    <button onClick={() => dispatch(decNumber())}>decrement</button>
    </>
  )
}

export default CounterFunction
