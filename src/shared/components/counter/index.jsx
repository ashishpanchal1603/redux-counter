import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
class Counter extends Component {
  render () {
    return (
            <>
   <button onClick={() => this.props.increment()} name="up">increment</button>
    <h1>{this.props.count}</h1>
    <button onClick={() => this.props.decrement()} name="down">decrement</button>
    </>
    )
  }
}

const mapStateToProps = (state) => {
  console.log('state :>> ', state)
  return {
    count: state.changeValue
  }
}
const mapDispatchToProps = (dispatch) => {
  console.log('dispatch', dispatch)
  return {
    increment: () => dispatch({ type: 'INCREMENT' }),
    decrement: () => dispatch({ type: 'DECREMENT' })

  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Counter)
Counter.propTypes = {
  increment: PropTypes.func,
  decrement: PropTypes.func,
  count: PropTypes.number

}
