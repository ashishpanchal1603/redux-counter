const incNumber = () => {
  console.log('increment')
  return ({
    type: 'INCREMENT'
  })
}
const decNumber = () => {
  console.log('decrement')
  return ({
    type: 'DECREMENT'
  })
}

export { incNumber, decNumber }
