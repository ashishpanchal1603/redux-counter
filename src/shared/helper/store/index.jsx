import { createStore } from 'redux'
import rootReducer from '../../constant/rootReducer'
const store = createStore(rootReducer)

export default store
