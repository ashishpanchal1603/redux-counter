import React, { Component } from 'react'
import Counter from './shared/components/counter'
import CounterFunction from './shared/components/counterFunction'

class App extends Component {
  render () {
    return (
      <>
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
      <div style={{ border: '1px solid black' }}>
        <h2>class Component</h2>
      <Counter/>
      </div>
      <div style={{ border: '1px solid black' }}>
      <h2>function Component</h2>
      <CounterFunction/>
      </div>
      </div>
    </>
    )
  }
}

export default App
